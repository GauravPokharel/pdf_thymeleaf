package com.pdfthymeleaf.pdfthymeleaf.services;

import com.lowagie.text.DocumentException;
import com.pdfthymeleaf.pdfthymeleaf.models.Person;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.*;

@Service
@AllArgsConstructor
public class PdfService {
    private final ClassLoaderTemplateResolver templateResolver;
    public String parseThymeleafTemplate(Person person) {
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        Context context = new Context();
        context.setVariable("person", person);

        return templateEngine.process("thymeleaf_template", context);
    }

    public void generatePdfFromHtml(String html) throws IOException, DocumentException {
        String outputFolder = "/home/gaurav/javaWorkspace" + File.separator + "thymeleaf.pdf";

        OutputStream outputStream = new FileOutputStream(outputFolder);
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(outputStream);
        outputStream.close();
    }
}
