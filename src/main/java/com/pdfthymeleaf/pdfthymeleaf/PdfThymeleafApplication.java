package com.pdfthymeleaf.pdfthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PdfThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(PdfThymeleafApplication.class, args);
    }

}
