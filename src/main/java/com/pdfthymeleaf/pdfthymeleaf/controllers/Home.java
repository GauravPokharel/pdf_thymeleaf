package com.pdfthymeleaf.pdfthymeleaf.controllers;

import com.pdfthymeleaf.pdfthymeleaf.models.Person;
import com.pdfthymeleaf.pdfthymeleaf.services.PdfService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class Home {
    private final PdfService pdfService;
    @GetMapping("/")
    public String index() throws Exception {
        Person person = new Person();
        person.setName("ABC");
        person.setAddress("KTM");
        person.setDescription("Hello! this is Mr.ABC.");
        String html = pdfService.parseThymeleafTemplate(person);
        pdfService.generatePdfFromHtml(html);
        return "Check: .../pdf-thymeleaf";
    }
}
